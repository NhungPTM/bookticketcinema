package bookticket;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@Controller
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "SignIn";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}