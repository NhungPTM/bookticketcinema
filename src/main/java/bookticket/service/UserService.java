package bookticket.service;

import bookticket.entities.User;

public interface UserService {
	Iterable<User> findAll();
	
	User findOne(int id);
	
	User save(User user);
	
	void delete(int id);
	
	User findByUserName(String userName);
	
	User findByEmail(String email);
	
	User findByPhone(String phone);
}
