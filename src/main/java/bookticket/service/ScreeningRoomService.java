package bookticket.service;

import bookticket.entities.ScreeningRoom;

public interface ScreeningRoomService {
	Iterable<ScreeningRoom> findAll();
	
	ScreeningRoom findOne(int id);
	
	ScreeningRoom save(ScreeningRoom sRoom);
	
	void delete(int id);
}
