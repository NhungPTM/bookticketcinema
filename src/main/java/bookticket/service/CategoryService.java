package bookticket.service;

import bookticket.entities.Category;

public interface CategoryService {
	Iterable<Category> findAll();
	
	Category findOne(int id);
	
	Category save(Category category);
	
	void delete(int id);
}
