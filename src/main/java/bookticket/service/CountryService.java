package bookticket.service;

import bookticket.entities.Country;

public interface CountryService {
	Iterable<Country> findAll();
	
	Country findOne(int id);
	
	Country save(Country country);
	
	void delete(int id);
}
