package bookticket.service;

import bookticket.entities.Employee;

public interface EmployeeService {
	
	Iterable<Employee> findAll();
	
	Employee findOne(int id);
	
	Employee save(Employee employee);
	
	void delete(int id);
	
}
