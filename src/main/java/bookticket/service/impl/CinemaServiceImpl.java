package bookticket.service.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import bookticket.entities.Cinema;
import bookticket.repository.CinemaRepository;
import bookticket.service.CinemaService;

@Service
public class CinemaServiceImpl implements CinemaService{
	
	@Autowired
	private CinemaRepository cinemaRepository;

	@Override
	public Iterable<Cinema> findAll() {
		return cinemaRepository.findAll();
	}

	@Override
	public Cinema findOne(int id) {
		return cinemaRepository.findOne(id);
	}

	@Override
	public Cinema save(Cinema cinema) {
		return cinemaRepository.save(cinema);
	}

	@Override
	public void delete(int id) {
		cinemaRepository.delete(id);
	}

	@Override
	public void saveImage(MultipartFile imageFile) throws Exception {
		Path currentPath = Paths.get(".");
		Path absolutePath = currentPath.toAbsolutePath();
		
		String filePath = absolutePath + "/src/main/resources/static/logo/";
		/* File file = new File(photoDTO.getPath()); */
		/*
		 * if(!file.exists()) { file.mkdirs(); }
		 */
		byte[] bytes = imageFile.getBytes();
		Path path = Paths.get(filePath + imageFile.getOriginalFilename());
		Files.write(path, bytes);
		
	}

}
