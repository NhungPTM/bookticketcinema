package bookticket.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bookticket.entities.ScreeningRoom;
import bookticket.repository.ScreeningRoomRepository;
import bookticket.service.ScreeningRoomService;

@Service
public class ScreeningRoomServiceImpl implements ScreeningRoomService{

	@Autowired
	private ScreeningRoomRepository sRoomRepository;
	
	@Override
	public Iterable<ScreeningRoom> findAll() {
		return sRoomRepository.findAll();
	}

	@Override
	public ScreeningRoom findOne(int id) {
		return sRoomRepository.findOne(id);
	}

	@Override
	public ScreeningRoom save(ScreeningRoom sRoom) {
		return sRoomRepository.save(sRoom);
	}

	@Override
	public void delete(int id) {
		sRoomRepository.delete(id);
	}

}
