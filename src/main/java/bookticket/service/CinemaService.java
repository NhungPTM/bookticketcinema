package bookticket.service;

import org.springframework.web.multipart.MultipartFile;

import bookticket.entities.Cinema;

public interface CinemaService {
	Iterable<Cinema> findAll();
	
	Cinema findOne(int id);
	
	Cinema save(Cinema cinema);
	
	void delete(int id);

	void saveImage(MultipartFile imageFile) throws Exception;
}
