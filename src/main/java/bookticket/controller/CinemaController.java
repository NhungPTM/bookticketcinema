package bookticket.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.Cinema;
import bookticket.service.CinemaService;

@Controller
public class CinemaController {
	
	@Autowired
	private CinemaService cinemaService;
	
	@GetMapping("/cinema")
	public ModelAndView listCinema(ModelAndView mv) {
		mv.addObject("cinemalist", cinemaService.findAll());
		mv.setViewName("AdminCinemaList");
		return mv;
	}
	
	@GetMapping("/cinema/add")
	public ModelAndView addCinema(ModelAndView mv) {
		mv.addObject("cinema", new Cinema());
		mv.setViewName("AddCinema");
		return mv;
	}
	
	@PostMapping("/cinema/save")
	public String saveCinema(@Valid Cinema cinema, BindingResult result, @RequestParam("imageFile") MultipartFile imageFile, RedirectAttributes redirect) {
		/*
		 * if(result.hasErrors()) { return "AddCinema"; }
		 */
		try {
			cinemaService.saveImage(imageFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cinema.setCinemaImage("/logo/"+imageFile.getOriginalFilename());
		System.out.println(cinema.getCinemaImage());
			
		cinemaService.save(cinema);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/cinema";
	}
	
	@GetMapping("/cinema/{id}/edit")
	public ModelAndView edit(@PathVariable("id") int cinemaId, ModelAndView mv) {
		mv.addObject("cinema", cinemaService.findOne(cinemaId));
		mv.setViewName("AddCinema");
		return mv;
	}
	
	@GetMapping("/cinema/{id}/delete")
	public String delete(@PathVariable("id") int cinemaId, RedirectAttributes redirect) {
		cinemaService.delete(cinemaId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/cinema";
	}
}
