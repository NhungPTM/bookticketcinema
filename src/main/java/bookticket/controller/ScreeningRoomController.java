package bookticket.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.ScreeningRoom;
import bookticket.service.ScreeningRoomService;

@Controller
public class ScreeningRoomController {
	
	@Autowired
	private ScreeningRoomService roomService;
	
	@GetMapping("/room")
	public ModelAndView roomList(ModelAndView mv) {
		mv.addObject("roomlist", roomService.findAll());
		mv.setViewName("RoomList");
		return mv;
	}
	
	@GetMapping("/room/add")
	public ModelAndView addRoom(ModelAndView mv) {
		mv.addObject("room", new ScreeningRoom());
		mv.setViewName("AddRoom");
		return mv;
	}
	
	@PostMapping("/room/save")
	public String saveScreeningRoom(@Valid @ModelAttribute("room") ScreeningRoom room, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return "AddRoom";
		}
		roomService.save(room);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/room";
	}
	
	@GetMapping("/room/{id}/edit")
	public ModelAndView edit(@PathVariable("id") int roomId, ModelAndView mv) {
		mv.addObject("room", roomService.findOne(roomId));
		mv.setViewName("AddRoom");
		return mv;
	}
	
	@GetMapping("/room/{id}/delete")
	public String delete(@PathVariable("id") int roomId, RedirectAttributes redirect) {
		roomService.delete(roomId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/room";
	}

}
