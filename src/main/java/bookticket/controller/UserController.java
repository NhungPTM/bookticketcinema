package bookticket.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.User;
import bookticket.service.UserService;

public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/user")
	public ModelAndView listUser(ModelAndView mv) {
		mv.addObject("userlist", userService.findAll());
		mv.setViewName("UserList");
		return mv;
	}
	
	@GetMapping("/user/add")
	public ModelAndView addUser(ModelAndView mv) {
		mv.addObject("user", new User());
		mv.setViewName("SignUp");
		return mv;
	}
	
	@PostMapping("/user/save")
	public String saveUser(@Valid User user, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return "AddUser";
		}
		User userNameExists = userService.findByUserName(user.getUserName());
		if (userNameExists != null) {
			result
					.rejectValue("userName", "error.user",
							"Duplicate user's username.");
		}
		
		User userEmailExists = userService.findByEmail(user.getEmail());
		if (userEmailExists != null) {
			result
					.rejectValue("email", "error.user",
							"Duplicate user's email.");
		}
		
		User userPhoneExists = userService.findByPhone(user.getPhone());
		if (userPhoneExists != null) {
			result
					.rejectValue("phone", "error.user",
							"Duplicate user's phone.");
		}
		userService.save(user);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/user";
	}
	
	@GetMapping("/user/{id}/edit")
	public ModelAndView edit(@PathVariable("id") int userId, ModelAndView mv) {
		mv.addObject("user", userService.findOne(userId));
		mv.setViewName("AddUser");
		return mv;
	}
	
	@GetMapping("/user/{id}/delete")
	public String delete(@PathVariable("id") int userId, RedirectAttributes redirect) {
		userService.delete(userId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/user";
	}
}
