package bookticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import bookticket.service.ScreeningRoomService;

@Controller
public class BookingTicketController {
	
	@Autowired
	private ScreeningRoomService roomService;
	
	// show seats
	@GetMapping("/booking")
	public ModelAndView booking(ModelAndView mv) {
		mv.addObject("seats", roomService.findOne(1));
		mv.addObject("alphabet", createAlphabet());
		mv.setViewName("view");
		return mv;
	}
	
	private char[] createAlphabet() {
		char alphabet[] = new char[26];
		int k = 0;
		for(int i = 65; i <= 90; i++) {
			alphabet[k] = (char)i;
			k++;
		}
		return alphabet;
	}
}
