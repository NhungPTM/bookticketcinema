package bookticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import bookticket.entities.Cinema;

@Mapper
public interface CinemaMapper {
	
	// get list cinemas in system
	final String GET_ALL_CINEMA = "SELECT * FROM Cinema";
	@Select(GET_ALL_CINEMA)
	@Results(value = { @Result(property = "cinemaId", column = "CinemaID"),
						@Result(property = "cinemaName", column = "CinemaName"),
						@Result(property = "cinemaAddress", column = " CinemaAddress")})
	public List<Cinema> getAllCinema();
	
	// get cinema by id
	final String GET_CINEMA_BY_ID = "SELECT * FROM Cinema WHERE CinemaID = #{cinemaId}";
	@Select(GET_CINEMA_BY_ID)
	public Cinema getById(@Param("cinemaId") Integer cinemaId);
	
	// insert cinema
	final String INSERT_CINEMA = "INSERT INTO Cinema (CinemaName, CinemaAddress) VALUES (#{cinemaName}, #{cinemaAddress})";
	@Insert(INSERT_CINEMA)
	@Options(useGeneratedKeys = true, keyProperty = "cinemaId")
	public void insert(Cinema cinema);
	
	// update cinema
	final String UPDATE_CINEMA = "UPDATE Cinema SET CinemaName = #{cinemaId}, CinemaAddress = #{cinemaAddress}";
	@Update(UPDATE_CINEMA)
	public void update(Cinema cinema);
	
	// delete cinema
	final String DELETE_CINEMA = "DELETE FROM Cinema WHERE CinemaID = #{cinemaId}";
	@Delete(DELETE_CINEMA)
	public void delete(Integer cinemaId);
}
