package bookticket.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "movie")
public class Movie implements Serializable{

	private static final long serialVersionUID = 7501251046768162896L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "MovieID")
	private Integer movieId;
	
	@Column(name = "MovieName")
	private String movieName;
	
	@Column(name = "Image")
	private String image;
	
	@Column(name = "StartDate")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@Column(name = "EndDate")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@Column(name = "Description")
	private String desciption;
	
	@Column(name = "Trailer")
	private String trailer;
	
	@Column(name = "Time")
	private Integer time;
	
	@Column(name = "Status")
	private String status;
	
	@Column(name = "actor")
	private String actor;
	
	@ManyToOne
	@JoinColumn(name="country_id")
	private Country country;
			
	@ManyToOne
	@JoinColumn(name="category_id")
	private Category category;
	
	public Movie() {
	}
	
	public Movie(Integer movieId, String movieName, String image, Date startDate, Date endDate, String desciption,
			String trailer, Integer time, String status) {
		super();
		this.movieId = movieId;
		this.movieName = movieName;
		this.image = image;
		this.startDate = startDate;
		this.endDate = endDate;
		this.desciption = desciption;
		this.trailer = trailer;
		this.time = time;
		this.status = status;
	}

	public Integer getMovieId() {
		return movieId;
	}

	public void setMovieId(Integer movieId) {
		this.movieId = movieId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public String getTrailer() {
		return trailer;
	}

	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		
		if(!(obj instanceof Movie)) {
			return false;
		}
		
		Movie movie = (Movie) obj;
		return Objects.equals(this.getMovieId(), movie.getMovieId());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.movieId);
	}
}
