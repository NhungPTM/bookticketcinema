package bookticket.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cinema")
public class Cinema implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6735797294551498199L;

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column(name = "cinema_id")
	private Integer cinemaId;
	
	@Column(name = "cinema_name")
	private String cinemaName;
	
	@Column(name = "cinema_image")
	private String cinemaImage;
	
	@Column(name = "cinema_address")
	private String cinemaAddress;
	
	@Column(name = "cinema_phone")
	private String cinemaPhone;
	
	@Column(name = "cinema_map")
	private String cinemaGGMap;
	
	@Column(name = "user_name")
	private String cinemaUserName;
	
	@Column(name = "password")
	private String cinemaPassword;
	
	@Column(name = "cinema_description")
	private String cinemaDescription;
	
	public Cinema() {
	}

	public Cinema(Integer cinemaId, String cinemaName, String cinemaImage, String cinemaAddress, String cinemaPhone,
			String cinemaGGMap, String cinemaUserName, String cinemaPassword, String cinemaDescription) {
		super();
		this.cinemaId = cinemaId;
		this.cinemaName = cinemaName;
		this.cinemaImage = cinemaImage;
		this.cinemaAddress = cinemaAddress;
		this.cinemaPhone = cinemaPhone;
		this.cinemaGGMap = cinemaGGMap;
		this.cinemaUserName = cinemaUserName;
		this.cinemaPassword = cinemaPassword;
		this.cinemaDescription = cinemaDescription;
	}

	public Integer getCinemaId() {
		return cinemaId;
	}

	public void setCinemaId(Integer cinemaId) {
		this.cinemaId = cinemaId;
	}

	public String getCinemaName() {
		return cinemaName;
	}

	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}

	public String getCinemaImage() {
		return cinemaImage;
	}

	public void setCinemaImage(String cinemaImage) {
		this.cinemaImage = cinemaImage;
	}

	public String getCinemaAddress() {
		return cinemaAddress;
	}

	public void setCinemaAddress(String cinemaAddress) {
		this.cinemaAddress = cinemaAddress;
	}

	public String getCinemaPhone() {
		return cinemaPhone;
	}

	public void setCinemaPhone(String cinemaPhone) {
		this.cinemaPhone = cinemaPhone;
	}

	public String getCinemaGGMap() {
		return cinemaGGMap;
	}

	public void setCinemaGGMap(String cinemaGGMap) {
		this.cinemaGGMap = cinemaGGMap;
	}

	public String getCinemaUserName() {
		return cinemaUserName;
	}

	public void setCinemaUserName(String cinemaUserName) {
		this.cinemaUserName = cinemaUserName;
	}

	public String getCinemaPassword() {
		return cinemaPassword;
	}

	public void setCinemaPassword(String cinemaPassword) {
		this.cinemaPassword = cinemaPassword;
	}

	public String getCinemaDescription() {
		return cinemaDescription;
	}

	public void setCinemaDescription(String cinemaDescription) {
		this.cinemaDescription = cinemaDescription;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		
		if(!(obj instanceof Cinema)) {
			return false;
		}
		
		Cinema cinema = (Cinema) obj;
		
		return Objects.equals(cinema.getCinemaId(), this.getCinemaId());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.cinemaId);
	}
}
