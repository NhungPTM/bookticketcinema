package bookticket.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "screening_room")
public class ScreeningRoom implements Serializable{
	
	private static final long serialVersionUID = -6880604610559033630L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "room_id")
	private int roomId;
	
	@Column(name = "room_name")
	
	private String roomName;
	
	@Column(name = "seats")
	private int seats;
	
	public ScreeningRoom() {
	}

	public ScreeningRoom(int roomId, String roomName, int seats) {
		super();
		this.roomId = roomId;
		this.roomName = roomName;
		this.seats = seats;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}
	
}
