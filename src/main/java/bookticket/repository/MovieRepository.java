package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Movie;

public interface MovieRepository extends CrudRepository<Movie, Integer>{

}
