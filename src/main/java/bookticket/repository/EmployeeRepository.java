package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{

}
