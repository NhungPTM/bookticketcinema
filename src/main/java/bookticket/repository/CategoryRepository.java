package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer>{

}
