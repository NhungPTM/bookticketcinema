package bookticket.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Role;

public interface RoleRepository extends CrudRepository<Role, Integer>{
	@Query("SELECT r FROM Role r WHERE role = :role")
	Role findByName(String role);
}
