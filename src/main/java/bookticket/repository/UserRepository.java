package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	User findByUserName(String userName);
	User findByEmail(String email);
	User findByPhone(String phone);
}
