package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Cinema;

public interface CinemaRepository extends CrudRepository<Cinema, Integer>{

}
