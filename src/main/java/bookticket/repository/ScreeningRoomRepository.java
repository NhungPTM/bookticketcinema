package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.ScreeningRoom;

public interface ScreeningRoomRepository extends CrudRepository<ScreeningRoom, Integer>{

}
